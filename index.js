
// 1) Create a API to filter by department.
// 2) Create a API to sort by salary.
// 3) Create a API to search by employee_id.
const express = require('express');
const app = express();

const data = [{
employee_id:'123',
first_name:"xyz",
last_name:"xyz",
department:"IT",
Address:"xyz",
hire_date:"01-02-2023",
dob:"01-02-2012",
joiningDate:"20-02-2023",
salary:'123'
}]


app.get('/getDept/:department',(req, res, next) => {
    try {
        const department = req.params.department;
        const findDep = data.find({department:"IT"})
        if(findDep.length === 0) {
    res.send("department not available")
}
    res.json(findDep)
    } catch (err) {
        next (err)
    }
})

app.get('/employeesSal', (req, res, next) => {
    try {
        const sortSalaryByEmp =  data.find().sort()
        res.json(sortSalaryByEmp)
    } catch (err) {
        next (err)
    }
})

app.get('/findEmployees/:employee_id', (req, res, next) => {
    try {
        const empId = req.params.department;
        const findEmpInData = data.find({employee_id:empId})
        if(findEmpInData.length === 0) {
            res.send("employee ID not available")
        }
        res.json(findEmpInData)
    } catch (err) {
        next (err)
    }
})

// server
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
